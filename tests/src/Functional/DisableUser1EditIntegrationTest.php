<?php

namespace Drupal\Tests\disable_user_1_edit\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the admin functionality of the module.
 *
 * @group disable_user_1_edit
 */
class DisableUser1EditIntegrationTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['disable_user_1_edit'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests if module works as expected before and after we toggle the disable.
   */
  public function testDisableToggle() {
    $admin_user = $this->drupalCreateUser([
      'administer users',
      'administer disable user 1 edit',
    ]);
    $this->drupalLogin($admin_user);
    $this->drupalGet('/user/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('/admin/config/people/disable_user_1_edit');

    // Test that it is possible to toggle it off, and that this has the desired
    // effect.
    $page = $this->getSession()->getPage();
    $page->fillField('disabled', TRUE);
    $page->pressButton('op');
    $this->drupalGet('/user/1/edit');
    $this->assertSession()->statusCodeEquals(200);
  }

}
